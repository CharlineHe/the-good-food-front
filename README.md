# the-good-food-front

## Build the Docker image
```
docker build -t the-good-food-front .
```

### Run the app
```
docker run --name goodfood-front -d -p 8080:80 the-good-food-front
```
